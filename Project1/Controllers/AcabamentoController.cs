using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Project1.Models;
using Microsoft.AspNetCore.Cors;
namespace Project1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("IT3Client")]
    public class AcabamentoController : ControllerBase
    {
        private readonly ProjectContext _context;

         public AcabamentoController(ProjectContext context)
        {
            _context = context;

            if (_context.Acabamentos.Count() == 0)
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all products.
               
            }
        }
        
     [HttpGet]
      [EnableCors("IT3Client")]
        public ActionResult<List<Acabamento>> GetAll()
        {
            return _context.Acabamentos.ToList();
        }

        [HttpGet("{id}", Name = "GetAcabamento")]
        public ActionResult<Acabamento> GetById(long id)
        {
            var item = _context.Acabamentos.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
         [EnableCors("IT3Client")]
        public IActionResult Create(Acabamento acabamento)
        {
            
            _context.Acabamentos.Add(acabamento);
            _context.SaveChanges();

            return CreatedAtRoute("GetProduct", new { id = acabamento.AcabamentoId }, acabamento);
        }

         [HttpDelete("{id}")]
          [EnableCors("IT3Client")]
        public IActionResult Delete(long id)
        {
            var a = _context.Acabamentos.Find(id);
            System.Diagnostics.Debug.WriteLine(a);
            if (a == null)
            {
               
                return NotFound();

            }

            _context.Acabamentos.Remove(a);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpPut("{id}")]
         [EnableCors("IT3Client")]
        public IActionResult Update(long id, Acabamento acabamento)
        {
            var a = _context.Acabamentos.Find(id);
            if (a == null)
            {
                return NotFound();
            }
            a.Nome = acabamento.Nome;
            

            _context.Acabamentos.Update(a);
            _context.SaveChanges();
            return NoContent();
        }
    }
}