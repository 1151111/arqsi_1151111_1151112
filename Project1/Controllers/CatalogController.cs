using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Project1.Models;
using Microsoft.AspNetCore.Cors;
namespace Project1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
     [EnableCors("IT3Client")]
    public class CatalogContoller : ControllerBase
    {
        private readonly ProjectContext _context;

         public CatalogContoller(ProjectContext context)
        {
            _context = context;

            if (_context.Catalogs.Count() == 0)
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all TodoItems.
               /* _context.Catalogs.Add(new Product() {produtoId = 1, Nome = "Produto 1",
                Descricao="Descrição do Produto 1" });
                _context.Catalogs.Add(new Product() {produtoId = 2, Nome = "Produto 2",
                Descricao="Descrição do Produto 2" });
                _context.SaveChanges();*/
            }
        }
/*
        private static List<Product> produtos = new List<Product>() {
            new Product() {produtoId = 1, Nome = "Produto 1",
                Descricao="Descrição do Produto 1" },
            new Product() {produtoId = 2, Nome = "Produto 2",
                Descricao="Descrição do Produto 2" },
            new Product() {produtoId = 3, Nome = "Produto 3",
                Descricao="Descrição do Produto 3" },
            new Product() {produtoId = 4, Nome = "Produto 4",
            Descricao="Descrição do Produto 4" },
            new Product() {produtoId = 5, Nome = "Produto 5",
            Descricao="Descrição do Produto 5" }
        }; */
     [HttpGet]
      [EnableCors("IT3Client")]
        public ActionResult<List<Catalog>> GetAll()
        {
            return _context.Catalogs.ToList();
        }

        [HttpGet("{id}", Name = "GetTodo")]
         [EnableCors("IT3Client")]
        public ActionResult<Catalog> GetById(long id)
        {
            var item = _context.Catalogs.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

       /* [HttpPost]
        public IActionResult Create(Catalog catalog)
        {
            _context.Catalogs.Add(catalog);
            _context.SaveChanges();

            return CreatedAtRoute("GetTodo", new { id = catalog.catalogId }, catalog);
        }*/
    }
}