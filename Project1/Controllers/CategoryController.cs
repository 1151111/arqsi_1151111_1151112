using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Project1.Models;
using Microsoft.AspNetCore.Cors;

namespace Project1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("IT3Client")]
    public class CategoryController : ControllerBase
    {
        private readonly ProjectContext _context;

         public CategoryController(ProjectContext context)
        {
            _context = context;

            
        }
        
     [HttpGet]
     [EnableCors("IT3Client")]
        public ActionResult<List<Category>> GetAll()
        {
            return _context.Categories.ToList();
        }

        [HttpGet("{id}", Name = "GetCategory")]
        [EnableCors("IT3Client")]
        public ActionResult<Category> GetById(long id)
        {
            var item = _context.Categories.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
         [EnableCors("IT3Client")]
        public IActionResult Create(Category category)
        {
            
            _context.Categories.Add(category);
            _context.SaveChanges();

            return CreatedAtRoute("GetProduct", new { id = category.CategoryId }, category);
        }

        [HttpDelete("{id}")]
         [EnableCors("IT3Client")]
        public IActionResult Delete(long id)
        {
            var c = _context.Categories.Find(id);
            System.Diagnostics.Debug.WriteLine(c);
            if (c == null)
            {
               
                return NotFound();

            }

            _context.Categories.Remove(c);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpPut("{id}")]
         [EnableCors("IT3Client")]
        public IActionResult Update(long id, Category category)
        {
            var c = _context.Categories.Find(id);
            if (c == null)
            {
                return NotFound();
            }
            c.Nome = category.Nome;
            

            _context.Categories.Update(c);
            _context.SaveChanges();
            return NoContent();
        }
    }
}