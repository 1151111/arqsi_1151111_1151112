using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Project1.Models;
using Microsoft.AspNetCore.Cors;

namespace Project1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("IT3Client")]
    public class DimensionController : ControllerBase
    {
        private readonly ProjectContext _context;

         public DimensionController(ProjectContext context)
        {
            _context = context;

            if (_context.Dimensions.Count() == 0)
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all products.
               
            }
        }
        
     [HttpGet]
      [EnableCors("IT3Client")]
        public ActionResult<List<Dimension>> GetAll()
        {
            return _context.Dimensions.ToList();
        }

        [HttpGet("{id}", Name = "GetDimension")]
        [EnableCors("IT3Client")]
        public ActionResult<Dimension> GetById(long id)
        {
            var item = _context.Dimensions.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
         [EnableCors("IT3Client")]
        public IActionResult Create(Dimension dimension)
        {
            
            _context.Dimensions.Add(dimension);
            _context.SaveChanges();

            return CreatedAtRoute("GetProduct", new { id = dimension.dimensionId }, dimension);
        }

         [HttpDelete("{id}")]
          [EnableCors("IT3Client")]
        public IActionResult Delete(long id)
        {
            var d = _context.Dimensions.Find(id);
            System.Diagnostics.Debug.WriteLine(d);
            if (d == null)
            {
               
                return NotFound();

            }

            _context.Dimensions.Remove(d);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpPut("{id}")]
         [EnableCors("IT3Client")]
        public IActionResult Update(long id, Dimension dimension)
        {
            var d = _context.Dimensions.Find(id);
            if (d == null)
            {
                return NotFound();
            }
            d.depth = dimension.depth;
            d.height = dimension.height;
            d.lenght = dimension.lenght;

            _context.Dimensions.Update(d);
            _context.SaveChanges();
            return NoContent();
        }
    }
}