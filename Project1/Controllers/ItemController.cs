using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Project1.Models;
using System;
using System.Collections;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Cors;
namespace Project1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("IT3Client")]
    public class ItemController : ControllerBase
    {
        private readonly ProjectContext _context;

        public ItemController(ProjectContext context)
        {
            _context = context;

            if (_context.Items.Count() == 0)
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all TodoItems.
                /* _context.Catalogs.Add(new Product() {produtoId = 1, Nome = "Produto 1",
                 Descricao="Descrição do Produto 1" });
                 _context.Catalogs.Add(new Product() {produtoId = 2, Nome = "Produto 2",
                 Descricao="Descrição do Produto 2" });
                 _context.SaveChanges();*/
            }
        }
        /*
                private static List<Product> produtos = new List<Product>() {
                    new Product() {produtoId = 1, Nome = "Produto 1",
                        Descricao="Descrição do Produto 1" },
                    new Product() {produtoId = 2, Nome = "Produto 2",
                        Descricao="Descrição do Produto 2" },
                    new Product() {produtoId = 3, Nome = "Produto 3",
                        Descricao="Descrição do Produto 3" },
                    new Product() {produtoId = 4, Nome = "Produto 4",
                    Descricao="Descrição do Produto 4" },
                    new Product() {produtoId = 5, Nome = "Produto 5",
                    Descricao="Descrição do Produto 5" }
                }; */
        [HttpGet]
        [EnableCors("IT3Client")]
        public ActionResult<List<Item>> GetAll()
        {
            return _context.Items.ToList();
        }

        [HttpGet("{id}", Name = "GetItem")]
        [EnableCors("IT3Client")]
        public ActionResult<Item> GetById(long id)
        {
            var item = _context.Items.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            Console.WriteLine("Vartype" + item.GetType());
            return item;
        }

        [HttpGet("prod/{id}", Name = "GetProductItem")]
        [EnableCors("IT3Client")]
        public List<Project1.Models.Item> GetByIdProd(long id)
        {

            List<Item> list = _context.Items.ToList();
            List<Project1.Models.Item> listProd = new List<Project1.Models.Item>();


            for (int i = 0; i < list.Count; i++)
            {
                if (id == ((Item)list.ElementAt(i)).ProductId1)
                {
                    listProd.Add(list.ElementAt(i));
                }

            }

            return listProd.ToList();
        }

        [HttpDelete("{id}")]
        [EnableCors("IT3Client")]
        public IActionResult Delete(long id)
        {
            var m = _context.Items.Find(id);
            System.Diagnostics.Debug.WriteLine(m);
            if (m == null)
            {
               
                return NotFound();

            }

            _context.Items.Remove(m);
            _context.SaveChanges();
            return NoContent();
        }


        [HttpPost]
        [EnableCors("IT3Client")]
        public IActionResult Create(Item item)
        {
            var pPrimario = _context.Products.Find(item.ProductId1);
            var pSecundario = _context.Products.Find(item.ProductId2);
            Console.WriteLine("pPrimario  " + item.ProductId1);
            Console.WriteLine("pSecundario  " + item.ProductId2);

            var pSdimL = _context.Dimensions.Find(pSecundario.DimensionId).lenght;
            Console.WriteLine("pSdimL  " + pSdimL);
            var pSdimH = _context.Dimensions.Find(pSecundario.DimensionId).height;
            Console.WriteLine("pSdimH  " + pSdimH);
            var pSdimD = _context.Dimensions.Find(pSecundario.DimensionId).depth;
            Console.WriteLine("pSdimD  " + pSdimD);
            float pSArea = pSdimL * pSdimH;
            Console.WriteLine("pSArea  " + pSArea);

            var nPDimension = _context.Restrictions.Find(pPrimario.Dimension);
            Console.WriteLine("nPDimension  " + nPDimension);
            var pPdimL = _context.Dimensions.Find(pPrimario.DimensionId).lenght;
            Console.WriteLine("pPdimL  "+pPdimL);
            var pPdimH = _context.Dimensions.Find(pPrimario.DimensionId).height;
            Console.WriteLine("pPdimH  "+pPdimH);
            var pPdimD = _context.Dimensions.Find(pPrimario.DimensionId).depth;
            Console.WriteLine("pPdimD  "+pPdimD);
                Console.WriteLine("pPrimario.EmptyArea "+pPrimario.EmptyArea);
            float difArea =(pPrimario.EmptyArea - pSArea);
            Console.WriteLine("difArea "+difArea);
            if ( difArea >= 0)
            {
                Console.WriteLine("entrei for!");
                //limitar largura

                System.Random rand = new System.Random((int)System.DateTime.Now.Ticks);
                int RandomNumber;
                RandomNumber = rand.Next(100000, 999999);

                Console.WriteLine("entrei for! R");
               // item.ConfigId = string.Format("{HHmmss}", System.DateTime.Now) + RandomNumber;
                pPrimario.EmptyArea -= pSArea;
                Console.WriteLine("entrei for! AU");
                _context.Products.Update(pPrimario);
                _context.Items.Add(item);
                _context.SaveChanges();
                return CreatedAtRoute("GetTodo", new { id = item.ItemId }, item);
            }
            return NotFound();
        }
    }
}