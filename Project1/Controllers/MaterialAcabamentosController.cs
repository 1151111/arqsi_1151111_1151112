using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Project1.Models;
using Microsoft.AspNetCore.Cors;
namespace Project1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("IT3Client")]
    public class MaterialAcabamentosController : ControllerBase
    {
        private readonly ProjectContext _context;

         public MaterialAcabamentosController(ProjectContext context)
        {
            _context = context;

          
        }
        
     [HttpGet]
      [EnableCors("IT3Client")]
        public ActionResult<List<Material_Acabamento>> GetAll()
        {
            return _context.MatAcabamentos.ToList();
        }

        [HttpGet("{id}", Name = "GetMaterial_Acabamento")]
        [EnableCors("IT3Client")]
        public ActionResult<Material_Acabamento> GetById(long id)
        {
            var item = _context.MatAcabamentos.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
         [EnableCors("IT3Client")]
        public IActionResult Create(Material_Acabamento matAcabamentos)
        {
            
            _context.MatAcabamentos.Add(matAcabamentos);
            _context.SaveChanges();

            return CreatedAtRoute("GetProduct", new { id = matAcabamentos.MaterialId }, matAcabamentos);
        }

         [HttpDelete("{id}")]
          [EnableCors("IT3Client")]
        public IActionResult Delete(long id)
        {
            var m = _context.MatAcabamentos.Find(id);
            System.Diagnostics.Debug.WriteLine(m);
            if (m == null)
            {
               
                return NotFound();

            }

            _context.MatAcabamentos.Remove(m);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpPut("{id}")]
         [EnableCors("IT3Client")]
        public IActionResult Update(long id, Material_Acabamento mAcabamento)
        {
            var m = _context.MatAcabamentos.Find(id);
            if (m == null)
            {
                return NotFound();
            }
            m.MaterialId = mAcabamento.MaterialId;
            m.AcabamentoId = mAcabamento.AcabamentoId;

            _context.MatAcabamentos.Update(m);
            _context.SaveChanges();
            return NoContent();
        }
    }
}