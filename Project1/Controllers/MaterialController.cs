using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Project1.Models;
using Microsoft.AspNetCore.Cors;
namespace Project1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("IT3Client")]
    public class MaterialController : ControllerBase
    {
        private readonly ProjectContext _context;

         public MaterialController(ProjectContext context)
        {
            _context = context;

            if (_context.Materials.Count() == 0)
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all products.
               
            }
        }
        
     [HttpGet]
      [EnableCors("IT3Client")]
        public ActionResult<List<Material>> GetAll()
        {
            return _context.Materials.ToList();
        }

        [HttpGet("{id}", Name = "GetMaterial")]
        public ActionResult<Material> GetById(long id)
        {
            var item = _context.Materials.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
         [EnableCors("IT3Client")]
        public IActionResult Create(Material material)
        {
            
            _context.Materials.Add(material);
            _context.SaveChanges();

            return CreatedAtRoute("GetProduct", new { id = material.MaterialId }, material);
        }

          [HttpDelete("{id}")]
           [EnableCors("IT3Client")]
        public IActionResult Delete(long id)
        {
            var m = _context.Materials.Find(id);
            System.Diagnostics.Debug.WriteLine(m);
            if (m == null)
            {
               
                return NotFound();

            }

            _context.Materials.Remove(m);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpPut("{id}")]
         [EnableCors("IT3Client")]
        public IActionResult Update(long id, Material material)
        {
            var m = _context.Materials.Find(id);
            if (m == null)
            {
                return NotFound();
            }
            m.Nome = material.Nome;

            _context.Materials.Update(m);
            _context.SaveChanges();
            return NoContent();
        }
    }
}