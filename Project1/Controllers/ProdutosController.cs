using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Project1.Models;
using System;
using Microsoft.AspNetCore.Cors;
namespace Project1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("IT3Client")]
    public class ProdutosController : ControllerBase
    {
        private readonly ProjectContext _context;

         public ProdutosController(ProjectContext context)
        {
            _context = context;
        }

     [HttpGet]
      [EnableCors("IT3Client")]
        public ActionResult<List<Product>> GetAll()
        {
            return _context.Products.ToList();
        }

        [HttpGet("{id}", Name = "GetProduct")]
         [EnableCors("IT3Client")]
        public ActionResult<Product> GetById(long id)
        {
            var item = _context.Products.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

         //Get: api/Produto/nome
        [HttpGet("nome=\"{nome}\"")]
         [EnableCors("IT3Client")]
        public IActionResult GetProdutoPorNome(string nome)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var produto = _context.Products.Select(p => new ProductDTO(p)).Where(p => p.Nome.Equals(nome, System.StringComparison.OrdinalIgnoreCase));

            if (produto == null)
            {
                return NotFound();
            }

            return Ok(produto);
        }

        [HttpPost]
         [EnableCors("IT3Client")]
        public IActionResult Create(Product product)
        {
            
             _context.Dimensions.Find(product.DimensionId);
           float h =_context.Dimensions.Find(product.DimensionId).height;
           float l = _context.Dimensions.Find(product.DimensionId).lenght;

            product.EmptyArea = h*l;
            Console.WriteLine("Area ->"+product.EmptyArea );
            Console.WriteLine("L ->"+l );
            Console.WriteLine("H ->"+h );

            _context.Products.Add(product);
            _context.SaveChanges();

            return CreatedAtRoute("GetProduct", new { id = product.productId }, product);
        }

        [HttpDelete("{id}")]
         [EnableCors("IT3Client")]
        public IActionResult Delete(long id)
        {
            var p = _context.Products.Find(id);
            System.Diagnostics.Debug.WriteLine(p);
            if (p == null)
            {
                System.Diagnostics.Debug.WriteLine("Errooooooooooooooou");
                return NotFound();

            }

            _context.Products.Remove(p);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpPut("{id}")]
         [EnableCors("IT3Client")]
        public IActionResult Update(long id, Product produto)
        {
            var p = _context.Products.Find(id);
            if (p == null)
            {
                return NotFound();
            }
            p.Nome = produto.Nome;
            p.Descricao= produto.Descricao;
            p.CategoryId = produto.CategoryId;
            p.Material_AcabamentoID= produto.Material_AcabamentoID;
            p.DimensionId = produto.DimensionId;
            p.RestrictionId = produto.RestrictionId;
            


            _context.Products.Update(p);
            _context.SaveChanges();
            return NoContent();
        }
    }
}