using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Project1.Models;
using Microsoft.AspNetCore.Cors;
namespace Project1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("IT3Client")]
    public class RestrictionController : ControllerBase
    {
        private readonly ProjectContext _context;

        public RestrictionController(ProjectContext context)
        {
            _context = context;
        }

        [HttpGet]
        [EnableCors("IT3Client")]
        public ActionResult<List<Restriction>> GetAll()
        {
            return _context.Restrictions.ToList();
        }

        [HttpGet("{id}", Name = "GetRestriction")]
        [EnableCors("IT3Client")]
        public ActionResult<Restriction> GetById(long id)
        {
            var item = _context.Restrictions.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
        [EnableCors("IT3Client")]
        public IActionResult Create(Restriction restriction)
        {

            _context.Restrictions.Add(restriction);
            _context.SaveChanges();

            return CreatedAtRoute("GetProduct", new { id = restriction.restrictionId }, restriction);
        }

        [HttpDelete("{id}")]
        [EnableCors("IT3Client")]
        public IActionResult Delete(long id)
        {
            var r = _context.Restrictions.Find(id);
            System.Diagnostics.Debug.WriteLine(r);
            if (r == null)
            {

                return NotFound();

            }

            _context.Restrictions.Remove(r);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpPut("{id}")]
        [EnableCors("IT3Client")]
        public IActionResult Update(long id, Restriction restriction)
        {
            var r = _context.Restrictions.Find(id);
            if (r == null)
            {
                return NotFound();
            }
            r.depthMax = restriction.depthMax;
            r.heightMax = restriction.heightMax;
            r.lenghtMax = restriction.lenghtMax;

            _context.Restrictions.Update(r);
            _context.SaveChanges();
            return NoContent();
        }
    }
}