using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Project1.Models;
using Microsoft.AspNetCore.Cors;
namespace Project1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("IT3Client")]
    public class SubCategoryController : ControllerBase
    {
        private readonly ProjectContext _context;

         public SubCategoryController(ProjectContext context)
        {
            _context = context;

            
        }
        
     [HttpGet]
      [EnableCors("IT3Client")]
        public ActionResult<List<SubCategory>> GetAll()
        {
            return _context.SubCategories.ToList();
        }

        [HttpGet("{id}", Name = "GetSubCategory")]
        [EnableCors("IT3Client")]
        public ActionResult<SubCategory> GetById(long id)
        {
            var item = _context.SubCategories.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
         [EnableCors("IT3Client")]
        public IActionResult Create(SubCategory subCategory)
        {
            
            _context.SubCategories.Add(subCategory);
            _context.SaveChanges();

            return CreatedAtRoute("GetProduct", new { id = subCategory.SubCategoryId }, subCategory);
        }

        [HttpDelete("{id}")]
         [EnableCors("IT3Client")]
        public IActionResult Delete(long id)
        {
            var c = _context.SubCategories.Find(id);
            System.Diagnostics.Debug.WriteLine(c);
            if (c == null)
            {
               
                return NotFound();

            }

            _context.SubCategories.Remove(c);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpPut("{id}")]
         [EnableCors("IT3Client")]
        public IActionResult Update(long id, SubCategory category)
        {
            var c = _context.SubCategories.Find(id);
            if (c == null)
            {
                return NotFound();
            }
            c.CategoryPrincipalId = category.CategoryPrincipalId;
            c.CategorySecondaryId = category.CategorySecondaryId;

            _context.SubCategories.Update(c);
            _context.SaveChanges();
            return NoContent();
        }
    }
}