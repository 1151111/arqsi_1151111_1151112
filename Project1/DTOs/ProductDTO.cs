using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project1.Models
{
    public class ProductDTO
    {
       public long productId{get;  set;}
        public string Nome{get; set;}
        public string Descricao{get; set;}
        public long Material_AcabamentoID { get; set; }
        public long DimensionId { get; set; }
        public long CategoryId { get; set; }
        public long RestrictionId { get; set; }
        
        public ProductDTO(Product p){
            productId = p.productId;
            Nome = p.Nome;
            Descricao = p.Descricao;
            Material_AcabamentoID = p.Material_AcabamentoID;
            DimensionId = p.DimensionId;
            CategoryId = p.CategoryId;
            RestrictionId = p.RestrictionId;
        }
    }
}