using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project1.Models
{
    public class Item
    {
        public long ItemId {get;  set;}
        public long ProductId1{get;  set;}
        //public Product Product1{get;  set;}
        public long ProductId2{get;  set;}
        //public Product Product2{get;  set;}

        public String ConfigId{get;  set;}

        public Boolean Optional{get;  set;}
        
    }
}
