using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project1.Models
{
    public class Product
    {
        public long productId { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public long Material_AcabamentoID { get; set; }
        public Material_Acabamento Material_Acabamento { get; set; }
        public long DimensionId { get; set; }
        public Dimension Dimension { get; set; }
        public long CategoryId { get; set; }
        public Category Category { get; set; }
        public long RestrictionId { get; set; }
        public Restriction Restriction { get; set; }

        public float EmptyArea { get; set; }

    }
}
