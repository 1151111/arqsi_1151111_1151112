using Microsoft.EntityFrameworkCore;

namespace Project1.Models
{
    public class ProjectContext : DbContext
    {
        public ProjectContext(DbContextOptions<ProjectContext> options)
            : base(options)
        {
        }
        
        public DbSet<Item> Items{get; set;}
        public DbSet<Acabamento> Acabamentos{get; set;}
        public DbSet<Material> Materials{get; set;}
        public DbSet<Material_Acabamento> MatAcabamentos{get; set;}
        public DbSet<Restriction> Restrictions{get; set;}
        public DbSet<Dimension> Dimensions{get; set;}
        public DbSet<SubCategory> SubCategories{get; set;}
        public DbSet<Category> Categories{get; set;}
        public DbSet<Product> Products { get; set; }
        public DbSet<Catalog> Catalogs{get; set;}
    }
}