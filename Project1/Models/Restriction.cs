using System;
using System.Collections.Generic;

namespace Project1.Models
{
    public class Restriction
    {
        public long restrictionId{get; set;}
        public float lenghtMax{get; set;}
        public float heightMax{get; set;}
        public float depthMax{get; set;}   
        public float lenghtMin{get; set;}
        public float heightMin{get; set;}
        public float depthMin{get; set;}    
        
    }
}