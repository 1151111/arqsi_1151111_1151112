using System;
using System.Collections.Generic;

namespace Project1.Models
{
    public class SubCategory
    {
        public long SubCategoryId{get; set;}
        public long CategoryPrincipalId{get; set;}
        public long CategorySecondaryId{get; set;}
        
    }
}