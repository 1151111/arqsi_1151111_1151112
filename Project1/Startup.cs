using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Project1.Models;

namespace Project1
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                //options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            //Server=localhost\SQLEXPRESS;Database=arqsi;Trusted_Connection=True;
            // Server=tcp:arqsi1.database.windows.net,1433;Initial Catalog=ARQSI;Persist Security Info=False;User ID=administrador;Password=Arqsiisep.;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;
                var connection = @"Server=tcp:arqsi1.database.windows.net,1433;Initial Catalog=ARQSI;Persist Security Info=False;User ID=administrador;Password=Arqsiisep.;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            services.AddDbContext<ProjectContext>
                (options => options.UseSqlServer(connection));

                services.AddCors(opt => {opt.AddPolicy("IT3Client",b =>b.WithOrigins("http://localhost:4200").AllowAnyMethod().AllowAnyHeader());});
            // BloggingContext requires
            // using EFGetStarted.AspNetCore.NewDb.Models;
            // UseSqlServer requires
            // using Microsoft.EntityFrameworkCore;
           
        }


        public void Configure(IApplicationBuilder app)
        {
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseMvc();
            app.UseCors("IT3Client");
        }
    }
}